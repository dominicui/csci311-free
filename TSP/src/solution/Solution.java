package solution;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

public class Solution {
	static int numberOfNodes;
	static Stack<Integer> stack;
	static int orignal_adjacency_matrix[][];

	// public Solution()
	// {
	// stack = new Stack<Integer>();
	// }

	// public static void Result(int node)
	// {
	//// System.out.println("the citys are visited as follows");
	//
	// int[] visited = new int[numberOfNodes + 1];
	// visited[0] = 1;
	// stack = new Stack<Integer>();
	// stack.push(1);
	// int element, dst = 0, i;
	// int min = Integer.MAX_VALUE;
	// boolean minFlag = false;
	// System.out.print(1 + "\t");
	//
	// while (!stack.isEmpty())
	// {
	//
	// i = 0;
	// min = Integer.MAX_VALUE;
	// while (i <= numberOfNodes)
	// {
	// element = stack.peek();
	// i = element;
	// if (adjacencyMatrix[element][i] > 0 && visited[i] == 0)
	// {
	// if (min > adjacencyMatrix[element][i])
	// {
	// min = adjacencyMatrix[element][i];
	// dst = i;
	// minFlag = true;
	// }
	// }
	// i++;
	// }
	// if (minFlag)
	// {
	// visited[dst] = 1;
	// stack.push(dst);
	// System.out.print(dst + "\t");
	// minFlag = false;
	// continue;
	// }
	// stack.pop();
	// }
	// }

	public static void Map() {
		int j;
		int temp;
		Random rand = new Random();

		System.out.println("Enter the number of nodes in the graph");
		Scanner scanner = new Scanner(System.in);
		numberOfNodes = scanner.nextInt();
		orignal_adjacency_matrix = new int[numberOfNodes + 1][numberOfNodes + 1];

		System.out.println("Enter the adjacency matrix");
		for (int i = 1; i <= numberOfNodes; i++) {
			for (j = 1; j <= numberOfNodes; j++) {
				if (i == j) {
					orignal_adjacency_matrix[i][j] = 0;
				} else {
					temp = rand.nextInt(Math.abs(10)) + 1;
					orignal_adjacency_matrix[i][j] = temp;
					orignal_adjacency_matrix[j][i] = temp;
				}

			}
			j++;
		}
		return;
	}

	public static void Print() {
		for (int i = 1; i <= numberOfNodes; i++) {
			for (int j = 1; j <= numberOfNodes; j++) {
				System.out.print(orignal_adjacency_matrix[i][j] + " ");
			}
			System.out.print("\n");
		}
	}

	public static void main(String args[]) {

		try {
			Map();
			Print();

			int g[][] = new int[numberOfNodes][numberOfNodes];
			for (int i = 0; i < numberOfNodes; i++) {
				for (int j = 0; j < numberOfNodes; j++) {
					g[i][j] = orignal_adjacency_matrix[i + 1][j + 1];
				}
			}
//
//			int graph[][] = { { 0, 3, 4, 2, 7 }, 
//					          { 3, 0, 4, 6, 3 }, 
//					          { 4, 4, 0, 5, 8 }, 
//					          { 2, 6, 5, 0, 6 },
//					          { 7, 3, 8, 6, 0 } };

			System.out.println("Minimum Spanning Tree: ");
			MST.primMST(g);

			System.out.println("Dynamic Programming solution:");
			System.out.println("Path: ");
			TSPDP test = new TSPDP(g);
			int distance = test.DP();
			System.out.println("Distance: ");
			System.out.println(distance);

		} catch (InputMismatchException inputMismatch) {
			System.out.println("Wrong Input format");
		}
	}
}