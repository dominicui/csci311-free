package solution;

 

public class MST {

	private static int vertices = Solution.numberOfNodes;

	public MST(int vertices) {
		this.vertices = vertices;
	}

	public static int minKey(int[] key, boolean[] mstSet) {

		int min = Integer.MAX_VALUE;
		int minIndex = -1;

		for(int v = 0; v < vertices; v++) {
			if(!mstSet[v] && key[v] <= min) {
				min = key[v];
				minIndex = v;
			}
		}

		return minIndex;
	}

	public static void printMST(int parent[], int n, int[][] graph) {
		System.out.println("Edge   Weight");

		for (int i = 1; i < vertices; i++) {
			System.out.println((parent[i] +1)  + " - " + (i + 1) + "  " + graph[i][parent[i]]);
		}
	}
	
	public static void primMST(int[][] graph) {

		int parent[] = new int[vertices];

		int key[] = new int [vertices];

		boolean mstSet[] = new boolean[vertices];

		for(int i = 0; i < vertices; i++) {
			key[i] = Integer.MAX_VALUE;
			mstSet[i] = false;
		}

		key[0] = 0;

		parent[0] = -1;

		for (int count = 0; count < vertices - 1; count++) {

			int u = minKey(key, mstSet);

			mstSet[u] = true;

			for (int v = 0; v < vertices; v++) {

				if (graph[u][v] != 0 && !mstSet[v] && graph[u][v] <=  key[v]) {

					parent[v]  = u;
					key[v] = graph[u][v];
				}
			}
		}


		printMST(parent, vertices, graph);
	}

	

}